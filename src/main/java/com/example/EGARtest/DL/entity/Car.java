package com.example.EGARtest.DL.entity;

import com.example.EGARtest.enums.CarCategory;
import com.example.EGARtest.enums.CarType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "brand", nullable = false)
    private String brand;
    @Column(name = "model", nullable = false)
    private String model;
    @Column(name = "car_category", nullable = false)
    @Enumerated(EnumType.STRING)
    private CarCategory category;
    @Column(name = "gov_number", nullable = false)
    private String govNumber;
    @Column(name = "car_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private CarType type;
    @Column(name = "car_year", nullable = false)
    private LocalDate year;
    @Column(name = "has_trailer", nullable = false)
    private boolean hasTrailer;
}
