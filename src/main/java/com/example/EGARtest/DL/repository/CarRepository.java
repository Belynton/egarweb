package com.example.EGARtest.DL.repository;

import com.example.EGARtest.DL.entity.Car;
import com.example.EGARtest.enums.CarCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    @Query("SELECT c FROM Car c WHERE (:brand = '' or c.brand = :brand) and " +
            "(:model = '' or c.model = :model) and (:category is null or c.category = :category) and " +
            "(:govNumber = '' or c.govNumber = :govNumber) and (:year is null or c.year = :year)")
    List<Car> findCarsByParameters(@Param("model") String model, @Param("category") CarCategory category,
                                   @Param("govNumber") String govNumber, @Param("brand") String brand, @Param("year") LocalDate year);
}
