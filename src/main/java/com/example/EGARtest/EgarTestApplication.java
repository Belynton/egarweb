package com.example.EGARtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EgarTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EgarTestApplication.class, args);
	}

}
