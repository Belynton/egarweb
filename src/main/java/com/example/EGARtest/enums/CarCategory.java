package com.example.EGARtest.enums;

import lombok.Getter;

@Getter
public enum CarCategory {
    A("A"), B("B"), C("C"), D("D"), M("M");

    private final String name;

    CarCategory(String name) {
        this.name = name;
    }

}
