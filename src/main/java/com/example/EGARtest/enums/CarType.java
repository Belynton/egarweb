package com.example.EGARtest.enums;

import lombok.Getter;

@Getter
public enum CarType {
    SEDAN("Седан"),
    LIMOUSINE("Лимузин"),
    HATCHBACK("Хэчбек"),
    LIFTBACK("Лифтбек"),
    WAGON("Универсал"),
    COUPE("Купе"),
    CABRIOLET("Кабриолет"),
    ROADSTER("Родстер"),
    TARGA("Тарга"),
    MINIVAN("Минивэн"),
    PICKUP("Пикап"),
    CROSSOVER("Кроссовер"),
    SPEEDSTER("Спидстер");

    private final String name;

    CarType(String name) {
        this.name = name;
    }
}
