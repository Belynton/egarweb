package com.example.EGARtest.mapper;

import com.example.EGARtest.DL.entity.Car;
import com.example.EGARtest.web.dto.CarRequestDto;
import com.example.EGARtest.web.dto.CarResponseDto;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CarMapper {

    private ModelMapper mapper;

    @PostConstruct
    private void init() {
        mapper.createTypeMap(CarRequestDto.class, Car.class);
        mapper.createTypeMap(Car.class, CarResponseDto.class);
    }

    public Car requestToEntity(CarRequestDto carRequestDto) {
        return mapper.map(carRequestDto, Car.class);
    }

    public CarResponseDto entityToResponse(Car car) {
        return mapper.map(car, CarResponseDto.class);
    }
}
