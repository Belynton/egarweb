package com.example.EGARtest.service;

import com.example.EGARtest.web.dto.CarRequestDto;
import com.example.EGARtest.web.dto.CarResponseDto;
import com.example.EGARtest.web.dto.FilterRequestDto;

import java.util.List;

public interface CarService {

    CarResponseDto create(CarRequestDto carRequestDto);

    CarResponseDto getById(Long id);

    List<CarResponseDto> getAll();

    List<CarResponseDto> getFiltered(FilterRequestDto filterRequestDto);

    CarResponseDto update(Long id, CarRequestDto carRequestDto);

    void delete(Long id);
}
