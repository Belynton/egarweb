package com.example.EGARtest.service.impl;

import com.example.EGARtest.DL.entity.Car;
import com.example.EGARtest.DL.repository.CarRepository;
import com.example.EGARtest.mapper.CarMapper;
import com.example.EGARtest.service.CarService;
import com.example.EGARtest.web.dto.CarRequestDto;
import com.example.EGARtest.web.dto.CarResponseDto;
import com.example.EGARtest.web.dto.FilterRequestDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CarServiceImpl implements CarService {

    private CarMapper carMapper;
    private CarRepository carRepository;

    @Override
    public CarResponseDto create(CarRequestDto carRequestDto) {
        var car = carMapper.requestToEntity(carRequestDto);
        return carMapper.entityToResponse(carRepository.save(car));
    }

    @Override
    public CarResponseDto getById(Long id) {
        return carRepository.findById(id)
                .map(car -> carMapper.entityToResponse(car))
                .orElse(new CarResponseDto());
    }

    @Override
    public List<CarResponseDto> getAll() {
        return carRepository.findAll().stream()
                .map(car -> carMapper.entityToResponse(car))
                .toList();
    }

    @Override
    public List<CarResponseDto> getFiltered(FilterRequestDto filterRequestDto) {
        System.out.println(filterRequestDto);
        return carRepository.findCarsByParameters(filterRequestDto.getModel(), filterRequestDto.getCategory(),
                        filterRequestDto.getGovNumber(), filterRequestDto.getBrand(), filterRequestDto.getYear())
                .stream()
                .map(car -> carMapper.entityToResponse(car))
                .toList();
    }

    @Override
    public CarResponseDto update(Long id, CarRequestDto carRequestDto) {
        if (carRepository.findById(id).isEmpty()) {
            return new CarResponseDto();
        }
        var car = carMapper.requestToEntity(carRequestDto);
        car.setId(id);
        return carMapper.entityToResponse(carRepository.save(car));
    }

    @Override
    public void delete(Long id) {
        var car = carRepository.findById(id).orElse(new Car());
        carRepository.delete(car);
    }
}
