package com.example.EGARtest.web.controller;

import com.example.EGARtest.enums.CarCategory;
import com.example.EGARtest.enums.CarType;
import com.example.EGARtest.service.CarService;
import com.example.EGARtest.web.dto.CarRequestDto;
import com.example.EGARtest.web.dto.FilterRequestDto;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/")
@AllArgsConstructor
public class CarController {

    private CarService carService;

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("cars", carService.getAll());
        model.addAttribute("filterRequestDto", new FilterRequestDto());
        model.addAttribute("carTypes", CarType.values());
        model.addAttribute("carCategories", CarCategory.values());
        return "index";
    }

    @GetMapping(value = "/filter")
    public String getFiltered(@ModelAttribute FilterRequestDto filterRequestDto, Model model) {
        model.addAttribute("cars", carService.getFiltered(filterRequestDto));
        model.addAttribute("carTypes", CarType.values());
        model.addAttribute("carCategories", CarCategory.values());
        return "index";
    }

    @GetMapping(value = "/create")
    public String create(Model model) {
        model.addAttribute("carRequestDto", new CarRequestDto());
        model.addAttribute("carTypes", CarType.values());
        model.addAttribute("carCategories", CarCategory.values());
        return "save";
    }

    @GetMapping(value = "/update/{id}")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("carRequestDto", new CarRequestDto());
        model.addAttribute("car", carService.getById(id));
        model.addAttribute("carTypes", CarType.values());
        model.addAttribute("carCategories", CarCategory.values());
        return "update";
    }

    @PostMapping(value = "/create")
    public String create(@Valid @ModelAttribute CarRequestDto carRequestDto) {
        carService.create(carRequestDto);
        return "redirect:/";
    }

    @PostMapping(value = "/update/{id}")
    public String update(@Valid @ModelAttribute CarRequestDto carRequestDto, @PathVariable Long id) {
        carService.update(id, carRequestDto);
        return "redirect:/";
    }

    @PostMapping(value = "/delete/{carId}")
    public String delete(@PathVariable Long carId) {
        carService.delete(carId);
        return "redirect:/";
    }
}
