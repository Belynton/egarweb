package com.example.EGARtest.web.dto;

import com.example.EGARtest.enums.CarCategory;
import com.example.EGARtest.enums.CarType;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CarRequestDto {

    private String brand;
    private String model;
    private CarCategory category;
    private String govNumber;
    private CarType type;
    private LocalDate year;
    private boolean hasTrailer;
}
