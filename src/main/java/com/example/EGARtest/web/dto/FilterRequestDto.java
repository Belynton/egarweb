package com.example.EGARtest.web.dto;

import com.example.EGARtest.enums.CarCategory;
import lombok.Data;

import java.time.LocalDate;

@Data
public class FilterRequestDto {
    private String brand;
    private String model;
    private CarCategory category;
    private String govNumber;
    private LocalDate year;
}
