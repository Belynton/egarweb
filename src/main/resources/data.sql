TRUNCATE TABLE car;

INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2019-01-01', false, 'AUDI', 'B', 'SEDAN', 'A242AA', 'B23');
INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2011-01-01', false, 'LADA', 'B', 'SEDAN', 'B623BB', '2109');
INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2011-01-01', false, 'BMW', 'B', 'SEDAN', 'K512KK', 'e30');
INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2012-01-01', false, 'LADA', 'B', 'SEDAN', 'A525BC', 'granta');
INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2006-01-01', false, 'TOYOYA', 'B', 'SEDAN', 'P963MX', 'corolla');
INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2019-01-01', false, 'BMW', 'B', 'SEDAN', 'X555XX', 'x5');
INSERT INTO car (car_year, has_trailer, brand, car_category, car_type, gov_number, model)
VALUES ('2021-01-01', false, 'MERCEDES', 'B', 'SEDAN', 'B625KA', 'benz');