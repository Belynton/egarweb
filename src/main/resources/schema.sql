DROP TABLE IF EXISTS car;

create table car
(
    car_year     date         not null,
    has_trailer  boolean      not null,
    id           serial   not null
        primary key,
    brand        varchar(255) not null,
    car_category varchar(255) not null
        constraint car_car_category_check
            check ((car_category)::text = ANY
                   ((ARRAY ['A'::character varying, 'B'::character varying, 'C'::character varying, 'D'::character varying, 'M'::character varying])::text[])),
    car_type     varchar(255) not null
        constraint car_car_type_check
            check ((car_type)::text = ANY
                   ((ARRAY ['SEDAN'::character varying, 'LIMOUSINE'::character varying, 'HATCHBACK'::character varying, 'LIFTBACK'::character varying, 'WAGON'::character varying, 'COUPE'::character varying, 'CABRIOLET'::character varying, 'ROADSTER'::character varying, 'TARGA'::character varying, 'MINIVAN'::character varying, 'PICKUP'::character varying, 'CROSSOVER'::character varying, 'SPEEDSTER'::character varying])::text[])),
    gov_number   varchar(255) not null,
    model        varchar(255) not null
);

alter table car
    owner to egardb;